/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

/**
 *
 * @author Rattanalak
 */
public class Triangle {
    private double base;
    private double heigth;
    
    public Triangle(double base,double heigth){
        this.base = base;
        this.heigth = heigth;
    }   
    public double calTriangle(){
        return 0.5 * base*heigth;
    }
    public double getBase(){
         return base;
    }
    public double getHeigth (){
         return heigth;
    }
    
    public void setTriangle(double base,double heigth){
        if (base <= 0 || heigth <= 0){
            System.out.println("Error: Base and Heigth must more than zero!!!");
            return;
        }
        this.base = base;
        this.heigth = heigth;
    }
    @Override
    public String toString(){
       return "Area of the tritangle (base = " + this.getBase()+ ", heigth = " + this.getHeigth()+ ") is " + this.calTriangle(); 
    }

}
