/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

/**
 *
 * @author Rattanalak
 */
public class Square {
    private double wide;
    
    public Square (double wide){
        this.wide = wide;
    }
    public double calSquare(){
        return wide * wide;
    }
    public double getWide(){
        return wide;
    }
    public void setWide (double wide){
        if (wide <= 0) {
            System.out.println("Error: Wide must more than Zero!!!");
            return ;
        }
        this.wide = wide;
    }
    @Override
    public String toString(){
        return "Area of the square (wide =" + this.getWide() + ") is " + this.calSquare();
    }
}
