/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

/**
 *
 * @author Rattanalak
 */
public class Rectangle {
    private double wide;
    private double length;
    
    public Rectangle(double wide,double length){
        this.length = length;
        this.wide = wide;
    }
    public double calRectangle(){
        return wide * length;
    }
    
    public double getLength(){
       return length;
    }
    public double getWide(){
       return wide;
    }
    public void setRect(double wide,double length){
        if (wide <= 0 || length <=0){
            System.out.println("Error: wide and length must more than zero!!!");
            return;
        }
        else if(wide >= length){
            System.out.println("Error: wide must less than length");
        }
        this.length = length;
        this.wide = wide;
    }
     public String toString(){
         return "Area of the rectangle (wide = " + this.getWide() + ", length = " + this.getLength() + ") is " + this.calRectangle();
     }
    
        
    
    
}
